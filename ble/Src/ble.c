/***************************************************************************//**
	@file		ble.c
	@brief		BC118 library.
********************************************************************************
	Driver used to control the BLE BC118 from Sierra Wireless.

	@note		Atollic TrueSTUDIO for STM32 Version 9.3.0.

	@note		STM32F3.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		February-2019.
*******************************************************************************/

#include "ble.h"

#include <string.h>

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/* Carriage return and new line: */
#define BLE_LF	'\n'
#define BLE_CR	'\r'


/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/**
 * Reply send by the bluetooth.
 */
typedef enum {

	BLE_CMD_RD_MELODY_V34 = 0x00,			/**< Firmware version indication. */
	BLE_CMD_RD_READY,						/**< Ready indication. */
	BLE_CMD_RD_OK,							/**< Successful indication. */
	BLE_CMD_RD_ERR,							/**< Error indication. */
	BLE_CMD_RD_CONNECTION,					/**< Connection. */
	BLE_CMD_RD_TRS_ON,						/**< Transparent mode enable. */
	BLE_CMD_RD_MAX,							/**< Limitation. */

} ble_cmd_rd_t;

/**
 * Reply send by the bluetooth when the disconnection ocucr.
 */
typedef enum {

	BLE_CMD_DISC_ERR = 0x00,				/**< Error. */
	BLE_CMD_DISC_TRS_OFF,					/**< Transparent mode was close. */
	BLE_CMD_DISC_DSC,						/**< Remotely initiated disconnection has succeeded. */
	BLE_CMD_DISC_MAX,						/**< Limitation. */

} ble_cmd_disc_t;

/**
 * Protocol definition.
 */
typedef enum {

	BLE_PROTOCOL_SOF = 0x55	,				/**< Start of frame. */
	BLE_PROTOCOL_ESCAPE = 0x56,				/**< Escape byte. */
	BLE_PROTOCOL_BUFFER_MAX = 25,			/**< Maximum number of data in the buffer. */

} ble_protocol_t;

/*******************************************************************************
	GLOBALS VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/**
 * Write commands to the 3.4 version.
 */
const char *BleCmdWrV34[BLE_CMD_WR_MAX] = {
	"\r",					/**< BLE_CMD_WR_END. */
	"rst\n\r",				/**< BLE_CMD_WR_RST. */
	"adv on\n\r",			/**< BLE_CMD_WR_ADV_ON. */
	"trs\n\r",				/**< BLE_CMD_WR_TRS. */
};

/**
 * Write commands to the 4.0 version and above.
 */
const char *BleCmdWrV40[BLE_CMD_WR_MAX] = {
	"\n",					/**< BLE_CMD_WR_END. */
	"rst\n",				/**< BLE_CMD_WR_RST. */
	"adv=on\n",				/**< BLE_CMD_WR_ADV_ON. */
	"trs\n",				/**< BLE_CMD_WR_TRS. */
};

/**
 * Commands used in the normal operation.
 */
const char *BleCmdRd[BLE_CMD_RD_MAX] = {
	"Melody Smart v3.4", 	/**< BLE_CMD_RD_MELODY_V34. */
	"READY",				/**< BLE_CMD_RD_READY. */
	"OK",					/**< BLE_CMD_RD_OK. */
	"ERR",					/**< BLE_CMD_RD_ERR. */
	"LDN=ON",				/**< BLE_CMD_RD_CONNECTION . */
	"TRS=ON",				/**< BLE_CMD_RD_TRS_ON. */
};

/**
 * Commands used in the transparent mode to identifier a disconnection.
 */
const char *BleCmdDisc[BLE_CMD_DISC_MAX] = {
	"ERR",					/**< BLE_CMD_DISC_ERR. */
	"TRS=OFF",				/**< BLE_CMD_DISC_TRS_OFF. */
	"DSC=RMT",				/**< BLE_CMD_DISC_DSC. */
};

/**
 * Escape bytes;
 */
const char BleEscapeBytes[] = {
	'\n',
	'\r'
};

/*******************************************************************************
	LOCAL PROTOTYPES:
*******************************************************************************/

/* General routines: */
static ble_error_t Ble_Uart_Transmit(const uint8_t *data, const uint32_t size);
static void Ble_Reset_Buffers(ble_handle_t *h);
static void Ble_Event(ble_handle_t *h, const ble_event_t event);

/* Receive data processing: */
static uint8_t Ble_Rx_Disc_Analyzer(ble_handle_t *h);
static void Ble_Decrypt(uint8_t *data, const uint16_t n);
static uint16_t Ble_Checksum(const uint8_t *data, const uint16_t n);
static uint8_t Ble_Rx_Protocol(ble_handle_t *h, const uint8_t data);
static void Ble_Rx_Messages(ble_handle_t *h, const uint8_t i);
static void Ble_Rx_Processing(ble_handle_t *h);

/* State machine routines: */
static void Ble_Handle_Reset(ble_handle_t *h);
static void Ble_Handle_Off(ble_handle_t *h);
static void Ble_Handle_Ready(ble_handle_t *h);
static void Ble_Handle_Check(ble_handle_t *h);;
static void Ble_Handle_Advertisement(ble_handle_t *h);
static void Ble_Handle_Transparent(ble_handle_t *h);

/***************************************************************************//**
	@brief		Routine used to transmit through UART.
	@param		data Data pointer.
	@param		size Data size.
	@return		Driver error code.
*******************************************************************************/

static ble_error_t Ble_Uart_Transmit(const uint8_t *data, const uint32_t size) {

	ble_error_t error = BLE_ERROR_OK;

	if (HAL_UART_Transmit(&BLE_UART_DRIVER, (uint8_t *)data, (uint32_t)size, BLE_UART_TIMEOUT) != HAL_OK) {
		error = BLE_ERROR_UART;
	}

	return error;

}

/***************************************************************************//**
	@brief		Useful routine to reset some variables.
	@param		h 		Driver handle structure.
	@return		None.
*******************************************************************************/

static void Ble_Reset_Buffers(ble_handle_t *h) {

	/* Ring buffer reset: */
	h->bufferRingRx.wr = 0;
	h->bufferRingRx.rd = 0;
	h->bufferRingRx.counter = 0;

	/* Normal buffer reset. */
	h->bufferIndex = 0;

}

/***************************************************************************//**
	@brief		Useful routine used to test the callback and send the event.
	@param		h 		Driver handle structure.
	@param		event 	Event.
	@return		None.
*******************************************************************************/

static void Ble_Event(ble_handle_t *h, const ble_event_t event) {

	if (h->callbackEvent != NULL) {
		h->callbackEvent(event);
	}

}

/***************************************************************************//**
	@brief		Disconnection analyzer.

	In the transparent mode, the protocol in the UART is proprietary.
	However, the Melody message can be sent in the UART and this routine
	is used to check the disconnection patern in the input buffer.
	Example of message received:
		0x55, 0x03, 0x01, ..., 'T', 'R', 'S', '=', 'O', 'N', '\n', '\r'
	In this case, some mechanism must be implemented in the proprietary protocol
	to prevent some piece of the frame to be interpreted like a disconnection
	message. Here, a escape mechanism was implemented. See the protocol
	documentation to more details.

	@todo ble Reference.

	@param		h Driver handle structure.
	@return		TRUE if a disconnection patern was found and FALSE of not.
*******************************************************************************/

static uint8_t Ble_Rx_Disc_Analyzer(ble_handle_t *h) {

	uint8_t disc = 0;
	uint32_t i, j, k;
	buffer_t *b;

	/* Ring buffer read index: */
	b = (buffer_t *)&h->bufferRingRx;
	/* Checking all disconnection messages: */
	for (i = 0; i < BLE_CMD_DISC_MAX; i++) {
		j = strlen(BleCmdDisc[i]);
		k = b->rd;
		while (j) {
			if (!k) k = b->counter;
			k--;
			if((b->data[k] != BLE_LF) && (b->data[k] != BLE_CR)) {
				if (b->data[k] == BleCmdDisc[i][j-1]) {
					j--;
					if (j == 0) {
						disc = 1;
						break;
					}
				}
				else {
					break;
				}
			}
		}
		/* Checking if the patern was found: */
		if (disc) {
			break;
		}
	}

	return disc;
}

/***************************************************************************//**
	@brief		Useful routine used to decrypt a block of bytes.
	@param		data 	Data pointer.
	@param		n 		Size to decrypt.
	@return		None.
*******************************************************************************/

static void Ble_Decrypt(uint8_t *data, const uint16_t n) {

}

/***************************************************************************//**
	@brief		Useful routine to calculate the checksum value.
	@param		data 	Data pointer.
	@param		n 		Size.
	@return		Result.
*******************************************************************************/

static uint16_t Ble_Checksum(const uint8_t *data, const uint16_t n) {

	return 0xAAAA;

}

/***************************************************************************//**
	@brief		Protocol processing.
	@param		h Driver handle structure.
	@return		TRUE to successful and FALSE in case of error.
*******************************************************************************/
static uint8_t Ble_Rx_Protocol(ble_handle_t *h, const uint8_t data) {

	uint8_t ret = 1;
	static uint16_t size;
	uint16_t check;

	/* Checking if is the escape byte: */
	if ((data == BLE_PROTOCOL_ESCAPE) && (!h->reg.flags.escape)) {
		/* Escape indication, so, the next byte is valid: */
		h->reg.flags.escape = 1;
	}
	else {
		h->reg.flags.escape = 0;
		/* Start of Frame (SOF) and Data Size (N): */
		if (h->bufferIndex < 2) {
			h->bufferRx[h->bufferIndex++] = data;
			if (h->bufferIndex == 2) {
				/*
				 * 1 byte to SOF, 1 byte to N, 1 byte to command, N bytes to
				 * data and 2 byte to check:
				 */
				size = 5 + h->bufferRx[h->bufferIndex-1];
				/* Decrypting if necessary: */
				if (h->reg.flags.encrypt) {
					Ble_Decrypt(h->bufferRx, 1);
				}
				/* Checking some limitations: */
				if ((h->bufferRx[0] != BLE_PROTOCOL_SOF) || (size > BLE_PROTOCOL_BUFFER_MAX)) {
					/* Error indication to break the connection: */
					ret = 0;
				}
			}
		}
		/* Data and Checksum: */
		else {
			h->bufferRx[h->bufferIndex++] = data;
			if (h->bufferIndex >= size) {
				/* Decrypting if necessary: */
				if (h->reg.flags.encrypt) {
					Ble_Decrypt(&h->bufferRx[2], size - 2);
				}
				/* Checksum: */
				check = Ble_Checksum(h->bufferRx, size - 2);
				if (check == (h->bufferRx[size-2] << 8 | h->bufferRx[size-1])) {
					/* Callback: */
					if (h->callbackProt != NULL) {
						/* N + CMD + DATA: */
						h->callbackProt(h->bufferRx[1], h->bufferRx[2], &h->bufferRx[3]);
					}
					h->bufferIndex = 0;
				}
				else {
					/* Error indication to break the connection: */
					ret = 0;
				}
			}
		}
	}

	return ret;

}

/***************************************************************************//**
	@brief		Useful routine to check the messages received.
	@param		h Driver handle structure.
	@see		MainApp
*******************************************************************************/

static void Ble_Rx_Messages(ble_handle_t *h, const uint8_t i) {

	/* Firmware version indication: */
	if (i == BLE_CMD_RD_MELODY_V34){
		/* In this case we need change the commands to the version 3.4: */
		for (uint32_t i = 0; i < BLE_CMD_WR_MAX; i++) {
			h->cmdWr[i] = (char *)BleCmdWrV34[i];
		}
	}
	/* Ready indication: */
	else if (i == BLE_CMD_RD_READY){
		h->reg.flags.ready = 1;
	}
	/* Successful indication: */
	else if (i == BLE_CMD_RD_OK) {
		h->reg.flags.ok = 1;
	}
	/* Error indication: */
	else if (i == BLE_CMD_RD_ERR){
		h->reg.flags.error = 1;
	}
	/* Error indication: */
	else if (i == BLE_CMD_RD_CONNECTION){
		h->reg.flags.connection = 1;
	}
	/* Transparent mode enable indication: */
	else if (i == BLE_CMD_RD_TRS_ON){
		h->reg.flags.ok = 1;
	}
	/* Others: */
	else {

	}

}

/***************************************************************************//**
	@brief		Data reception processing.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Ble_Rx_Processing(ble_handle_t *h) {

	uint8_t data, i, protocol;

	/* Checking data in the rx ring buffer: */
	while (h->bufferRingRx.counter) {
		BufferRead((buffer_t *)&h->bufferRingRx, &data, 1);
		/**
		 * Checking the transparent mode:
		 *
		 * @note	When in transparent mode, MelodySmart will transparently
		 * 			forward data to and from UART over the air via a proprietary
		 * 			protocol.
		 */
		if (h->reg.flags.trs) {
			protocol = 0;
			/* Checking if a disconnection was done: */
			if (data == h->cmdWr[BLE_CMD_WR_END][0]) {
				/* Disconnection: */
				if (Ble_Rx_Disc_Analyzer(h)) {
					h->reg.flags.connection = 0;
				}
				/* Protocol processing: */
				else {
					protocol = 1;
				}
			}
			/* Protocol processing: */
			else {
				protocol = 1;
			}
			if ((protocol) && (!h->reg.flags.error)) {
				if (!Ble_Rx_Protocol(h, data)) {
					/*
					 * When a error is detected, we need wait a moment to
					 * break the connection, because a proper disconnection
					 * message might coming. In this case, we need to break
					 * normally.
					 */
					h->reg.flags.error = 1;
					h->counter = BLE_PERIOD_DISC;
				}
			}
		}
		/* So, is the normal mode: */
		else {
			/* Getting data: */
			if (h->bufferIndex < BLE_BUFFER_RX_SIZE) {
				/*
				 * Checking the termination indication.
				 *
				 * The first message received is the Melody firmware. So,
				 * in the first time (with ready = 0) the both possibilities
				 * of termination must be check, because the patern change
				 * according with the version.
				 */
				if (((h->reg.flags.ready == 1) && (data == h->cmdWr[BLE_CMD_WR_END][0])) ||
					((h->reg.flags.ready == 0) && ((data == BLE_LF) || (data == BLE_CR)))
				) {
					h->bufferRx[h->bufferIndex] = 0;
					for (i = 0; i < BLE_CMD_RD_MAX; i++) {
						if (strncmp(BleCmdRd[i], (char *)h->bufferRx, strlen(BleCmdRd[i])) == 0) {
							break;
						}
					}
					Ble_Rx_Messages(h, i);
					h->bufferIndex = 0;
				}
				else {
					/* In LF and CR is not necessary: */
					if ((data != BLE_LF) || (data != BLE_CR)) {
						h->bufferRx[h->bufferIndex++] = data;
					}
				}
			}
			else {
				/* Event just to debug purpose: */
				Ble_Event(h, BLE_EVENT_ERROR_FIFO);
			}
		}
	}
}

/***************************************************************************//**
	@brief		Reset state.

	Reset the module if necessary.

	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Ble_Handle_Reset(ble_handle_t *h) {

	/* Reset command: */
	if (Ble_Uart_Transmit((uint8_t *)h->cmdWr[BLE_CMD_WR_RST], strlen(h->cmdWr[BLE_CMD_WR_RST])) != BLE_ERROR_OK) {
		Ble_Event(h, BLE_EVENT_ERROR_UART);
	}

	/* Reset variables: */
	Ble_Reset_Buffers(h);
	h->reg.flags.ready = 0;
	h->reg.flags.en = 0;
	h->counter = BLE_PERIOD_READY;
	h->currentState = BLE_HANDLE_STATE_OFF;

}

/***************************************************************************//**
	@brief		Off state.

	Wait the initialization message.

	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Ble_Handle_Off(ble_handle_t *h) {

	/* Error verification: */
	if (!h->reg.flags.error) {
		/*
		 *  Checking the reception of the ready message with firmware
		 *  identification to choice the control commands:
		 */
		if (h->reg.flags.ready) {
			Ble_Event(h, BLE_EVENT_READY);
			h->currentState = BLE_HANDLE_STATE_READY;
		}
		else {
			/* If not, the module must be reset after the timeout: */
			if (!h->counter) {
				Ble_Event(h, BLE_EVENT_ERROR_INIT);
				h->currentState = BLE_HANDLE_STATE_RESET;
			}
		}
	}
	/* Error: */
	else {
		h->reg.flags.error = 0;
		Ble_Event(h, BLE_EVENT_ERROR_UART);
		h->currentState = BLE_HANDLE_STATE_RESET;
	}

}

/***************************************************************************//**
	@brief		Ready state.

	Ready to start.

	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Ble_Handle_Ready(ble_handle_t *h) {

	/* Error verification: */
	if (!h->reg.flags.error) {
		/* Checking advertisement request: */
		if (h->reg.flags.en) {
			/* Advertisement command: */
			if (Ble_Uart_Transmit((uint8_t *)h->cmdWr[BLE_CMD_WR_ADV_ON], strlen(h->cmdWr[BLE_CMD_WR_ADV_ON])) != BLE_ERROR_OK) {
				Ble_Event(h, BLE_EVENT_ERROR_UART);
				h->currentState = BLE_HANDLE_STATE_RESET;
			}
			else {
				/* Wait for answer: */
				h->reg.flags.ok = 0;
				h->reg.flags.connection = 0;
				h->event = BLE_EVENT_ADV_START;
				h->counter = BLE_PERIOD_OK;			/* Timeout to receive the reply. */
				h->nextCounter = BLE_PERIOD_ADV;	/* Counter related to the desired state. */
				h->currentState = BLE_HANDLE_STATE_CHECK;
				h->nextState = BLE_HANDLE_STATE_ADVERTISEMENT;
			}
		}
	}
	/* Error: */
	else {
		h->reg.flags.error = 0;
		Ble_Event(h, BLE_EVENT_ERROR_UART);
		h->currentState = BLE_HANDLE_STATE_RESET;
	}

}

/***************************************************************************//**
	@brief		Check state.

	Wait some response.

	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Ble_Handle_Check(ble_handle_t *h) {

	/* Error verification: */
	if (!h->reg.flags.error) {
		/* Checking if the confirmation was received: */
		if (h->reg.flags.ok) {
			if (h->event != BLE_EVENT_MAX) {
				Ble_Event(h, h->event);
			}
			h->counter = h->nextCounter;
			h->currentState = h->nextState;
		}
		else {
			/* If not, the module must be reset after the timeout: */
			if (!h->counter) {
				Ble_Event(h, BLE_EVENT_ERROR_CHECK);
				h->currentState = BLE_HANDLE_STATE_RESET;
			}
		}
	}
	/* Error: */
	else {
		h->reg.flags.error = 0;
		Ble_Event(h, BLE_EVENT_ERROR_UART);
		h->currentState = BLE_HANDLE_STATE_RESET;
	}

}

/***************************************************************************//**
	@brief		Advertisement state.

	Device is in the advertisement.

	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Ble_Handle_Advertisement(ble_handle_t *h) {

	/* Error verification: */
	if (!h->reg.flags.error) {
		/* Checking if the connection was done: */
		if (h->reg.flags.connection) {
			/* Connection command: */
			if (Ble_Uart_Transmit((uint8_t *)h->cmdWr[BLE_CMD_WR_TRS], strlen(h->cmdWr[BLE_CMD_WR_TRS])) != BLE_ERROR_OK) {
				Ble_Event(h, BLE_EVENT_ERROR_UART);
				h->currentState = BLE_HANDLE_STATE_RESET;
			}
			else {
				/* Ring buffer reset: */
				h->bufferRingRx.wr = 0;
				h->bufferRingRx.rd = 0;
				h->bufferRingRx.counter = 0;
				/* Wait for answer: */
				h->reg.flags.ok = 0;
				h->reg.flags.trs = 0;
				h->event = BLE_EVENT_CONNECTION;
				h->counter = BLE_PERIOD_OK;
				h->nextCounter = 0;
				h->currentState = BLE_HANDLE_STATE_CHECK;
				h->nextState = BLE_HANDLE_STATE_TRANSPARENT;
			}
		}
		else {
			/*
			 * If not, the module the advertisement must be done again after the
			 * timeout:
			 */
			if (!h->counter) {
				Ble_Event(h, BLE_EVENT_ADV_STOP);
				h->reg.flags.en = 0;
				h->currentState = BLE_HANDLE_STATE_READY;
			}
		}
	}
	/* Error: */
	else {
		h->reg.flags.error = 0;
		Ble_Event(h, BLE_EVENT_ERROR_UART);
		h->currentState = BLE_HANDLE_STATE_RESET;
	}

}

/***************************************************************************//**
	@brief		Transparent state.

	Device is the transparent mode.

	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Ble_Handle_Transparent(ble_handle_t *h) {

	/* Checking a error in the protocol reception: */
	if ((h->reg.flags.error) && (!h->counter)) {
		h->reg.flags.en = 0;
		h->reg.flags.error = 0;
		h->reg.flags.trs = 0;
		Ble_Event(h, BLE_EVENT_ERROR_PROTOCOL);
		h->currentState = BLE_HANDLE_STATE_RESET;
	}
	else {
		/* Checking if the connection is still in progress: */
		if (h->reg.flags.connection) {
			h->reg.flags.trs = 1;
		}
		else {
			Ble_Event(h, BLE_EVENT_DISCONNECTION);
			h->reg.flags.en = 0;
			h->reg.flags.error = 0;
			h->reg.flags.trs = 0;
			h->currentState = BLE_HANDLE_STATE_READY;
		}
	}

}

/***************************************************************************//**
	@brief		Driver initialization.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

void Ble_Init(ble_handle_t *h) {

	static uint8_t bufferRingRx[BLE_BUFFER_RING_RX_SIZE];

	h->reg.value = 0;
	h->currentState = BLE_HANDLE_STATE_OFF;
	h->nextState = BLE_HANDLE_STATE_OFF;
	h->counter = BLE_PERIOD_READY;
	h->callbackEvent = NULL;
	h->callbackProt = NULL;
	BufferReset((buffer_t *)&h->bufferRingRx, bufferRingRx, BLE_BUFFER_RING_RX_SIZE);
	h->bufferIndex = 0;
	for (uint32_t i = 0; i < BLE_CMD_WR_MAX; i++) {
		h->cmdWr[i] = (char *)BleCmdWrV40[i];
	}

}

/***************************************************************************//**
	@brief		Driver time base. Must be run in a 10ms routine.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

void Ble_Tick(ble_handle_t *h) {

	if (h->counter) h->counter--;

}

/***************************************************************************//**
	@brief		Driver manager.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

void Ble_Handle(ble_handle_t *h) {

	static void (* const fnc[])(ble_handle_t *h) = {
		Ble_Handle_Reset,			/* BLE_HANDLE_STATE_RESET. */
		Ble_Handle_Off,				/* BLE_HANDLE_STATE_OFF. */
		Ble_Handle_Ready,			/* BLE_HANDLE_STATE_READY. */
		Ble_Handle_Check,			/* BLE_HANDLE_STATE_CHECK. */
		Ble_Handle_Advertisement,	/* BLE_HANDLE_STATE_ADVERTISEMENT. */
		Ble_Handle_Transparent,		/* BLE_HANDLE_STATE_TRANSPARENT. */
	};

	/* Reception processing: */
	Ble_Rx_Processing(h);

	/* Manager state machine: */
	if (h->currentState < (sizeof (fnc)/sizeof(fnc[h->currentState]))) {
		fnc[h->currentState](h);
	}

}

/***************************************************************************//**
	@brief		Routine used to put the data in the FIFO.
	@param		h		Driver handle structure.
	@param		data 	RX data.
	@return		None.
*******************************************************************************/

void Ble_Uart_Receive(ble_handle_t *h, const uint8_t data) {

	BufferWrite((buffer_t *)&h->bufferRingRx, (uint8_t *)&data, 1);

}

//----------------------------------------------------------------------------//

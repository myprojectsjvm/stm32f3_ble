/***************************************************************************//**
	@file		app.c
	@brief		Firmware used to test the BC118.
********************************************************************************
	Firmware used to test the BC118.

	@note		Atollic TrueSTUDIO for STM32 Version 9.3.0.

	@note		STM32F3.

	@see		http://www.doxygen.nl/index.html

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		February-2019.
*******************************************************************************/

#include "app.h"
#include "includes.h"
#include "lodia.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/* Switch debounce (50ms): */
#define APP_SW_DEBOUNCE_PERIOD	50

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/**
 *
 */
typedef struct {

	union {
		uint8_t value;					/**< Value. */
		struct {
			uint8_t rqst10ms 	: 1;	/**< Reserved. */
			uint8_t bit1 		: 1;	/**< Reserved. */
			uint8_t bit2 		: 1;	/**< Reserved. */
			uint8_t bit3 		: 1;	/**< Reserved. */
			uint8_t bit4 		: 1;	/**< Reserved. */
			uint8_t bit5 		: 1;	/**< Reserved. */
			uint8_t bit6 		: 1;	/**< Reserved. */
			uint8_t bit7 		: 1;	/**< Reserved. */
		} flags;
	} reg;

	volatile uint8_t uartDataRx;
	uint32_t indicatioPeriod;
	volatile uint32_t indicationCounter;
	volatile uint32_t swDebounce;
	lodia_handle_t lodia;

} app_handle_t;

/*******************************************************************************
	GLOBALS VARIABLES:
*******************************************************************************/

app_handle_t AppHandle;

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	LOCAL PROTOTYPES:
*******************************************************************************/

void BleCallbackEvent(ble_event_t event);

/***************************************************************************//**
	@brief		BC118 callback event.
	@param		Event.
	@return		None.
*******************************************************************************/

void BleCallbackEvent(ble_event_t event) {

	/* Device is ready indication. */
	if (event == BLE_EVENT_READY) {
		AppHandle.indicatioPeriod = 0;
	}
	/* Indication that advertisement is in process. */
	else if (event == BLE_EVENT_ADV_START) {
		AppHandle.indicatioPeriod = 500;
	}
	/* Indication that advertisement was stopped (timeout). */
	else if (event == BLE_EVENT_ADV_STOP) {
		AppHandle.indicatioPeriod = 0;
	}
	/* Connection event. */
	else if (event == BLE_EVENT_CONNECTION) {
		AppHandle.indicatioPeriod = 250;
	}
	/* Disconnection event. */
	else if (event == BLE_EVENT_DISCONNECTION) {
		AppHandle.indicatioPeriod = 0;
	}
	/* Error. */
	else {
		AppHandle.indicatioPeriod = 100;
	}

}

/***************************************************************************//**
	@brief		Main application routine.
	@param		None.
	@return		None.
*******************************************************************************/

void App_Main(void) {

	/* Initializations: */
	HAL_TIM_Base_Start_IT(&htim1);
	HAL_UART_Receive_IT(&BLE_UART_DRIVER, (uint8_t *)&AppHandle.uartDataRx, 1);
	Lodia_Init(&AppHandle.lodia);
	AppHandle.lodia.ble.callbackEvent = BleCallbackEvent;

	/* Main loop: */
	while (1) {

		/* Indication: */
		if (!AppHandle.indicationCounter) {
			AppHandle.indicationCounter = AppHandle.indicatioPeriod;
			HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
		}

		/* Lodia manager: */
		Lodia_Handle(&AppHandle.lodia);

		/* 10ms: */
		if (AppHandle.reg.flags.rqst10ms) {
			AppHandle.reg.flags.rqst10ms = 0;

			/* Lodia driver time base: */
			Lodia_Tick(&AppHandle.lodia);

		}

		HAL_UART_Receive_IT(&BLE_UART_DRIVER, (uint8_t *)&AppHandle.uartDataRx, 1);

	}

}

/***************************************************************************//**
	@brief		Timer interrupt callback.
	@param		htim Timer structure.
	@return		None.
*******************************************************************************/

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {

	static uint32_t t1s = 0;
	static uint32_t t10ms = 0;

	if (AppHandle.indicationCounter) AppHandle.indicationCounter--;
	if (AppHandle.swDebounce) AppHandle.swDebounce--;

	/* 10ms delay: */
	if (++t10ms >= 10) {
		t10ms = 0;
		if (!AppHandle.reg.flags.rqst10ms) AppHandle.reg.flags.rqst10ms = 1;
	}

	/* 1s delay: */
	if (++t1s >= 1000) {
		t1s = 0;
	}

}

/***************************************************************************//**
	@brief		External interrupt callback.
	@param		GPIO_Pin pin.
	@return		None.
*******************************************************************************/

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {

	if (!AppHandle.swDebounce) {

		AppHandle.swDebounce = APP_SW_DEBOUNCE_PERIOD;

		HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);

		if (AppHandle.lodia.ble.reg.flags.en) AppHandle.lodia.ble.reg.flags.en = 0;
		else AppHandle.lodia.ble.reg.flags.en = 1;

	}

}

/***************************************************************************//**
	@brief		UART RX callback.
	@param		huart Uart structure.
	@return		None.
*******************************************************************************/

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {

	Ble_Uart_Receive(&AppHandle.lodia.ble, AppHandle.uartDataRx);

	HAL_UART_Receive_IT(&BLE_UART_DRIVER, (uint8_t *)&AppHandle.uartDataRx, 1);

}

//----------------------------------------------------------------------------//

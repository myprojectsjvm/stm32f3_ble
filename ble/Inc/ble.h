/***************************************************************************//**
	@file		ble.h
	@brief		BC118 library.
********************************************************************************
	Driver used to control the BLE BC118 from Sierra Wireless.

	@note		Atollic TrueSTUDIO for STM32 Version 9.3.0.

	@note		STM32F3.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		February-2019.
*******************************************************************************/

#ifndef BLE_H_
#define BLE_H_

#include "includes.h"
#include "buffer.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/**
 * STM32 UARTS driver.
 */
#define BLE_UART_DRIVER			huart2

/**
 * 100ms of timeout used in the UART.
 */
#define BLE_UART_TIMEOUT		100

/**
 * Size of the ring buffer used in the UART reception.
 *
 * @note Must be equal or lower than 0xFFFF.
 */
#define BLE_BUFFER_RING_RX_SIZE	32

/**
 * Size of the buffer used in the reception processing.
 *
 * @note Must be equal or lower than 0xFFFF.
 */
#define BLE_BUFFER_RX_SIZE		128

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/**
 * Time to receive the ready message [x10ms].
 */
#define BLE_PERIOD_READY		700

/**
 * Time to receive a confirmation [x10ms].
 */
#define BLE_PERIOD_OK			200

/**
 * Time in the advertisement that is defined when the module is configured [x10ms].
 */
#define BLE_PERIOD_ADV			1500

/**
 * Time used in the disconnection process.
 */
#define BLE_PERIOD_DISC			50

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/**
 * Driver error codes.
 */
typedef enum {

	BLE_ERROR_OK = 0x00,					/**< Successful indication. */
	BLE_ERROR_UART,							/**< UART driver error indication. */

} ble_error_t;

/**
 * Driver states.
 */
typedef enum {

	BLE_HANDLE_STATE_RESET = 0x00,			/**< Reset the module if necessary. */
	BLE_HANDLE_STATE_OFF,					/**< Wait the initialization message. */
	BLE_HANDLE_STATE_READY,					/**< Ready to start. */
	BLE_HANDLE_STATE_CHECK,					/**< Wait some response. */
	BLE_HANDLE_STATE_ADVERTISEMENT,			/**< Device is in the advertisement. */
	BLE_HANDLE_STATE_TRANSPARENT,			/**< Device is the transparent mode. */

} ble_handle_state_t;

/**
 * Events.
 */
typedef enum {

	BLE_EVENT_READY = 0x00,					/**< Device is ready. */
	BLE_EVENT_ADV_START,					/**< Indication that advertisement is in process. */
	BLE_EVENT_ADV_STOP,						/**< Indication that advertisement was stopped (timeout). */
	BLE_EVENT_CONNECTION,					/**< Connection event. */
	BLE_EVENT_DISCONNECTION,				/**< Disconnection event. */
	BLE_EVENT_ERROR_INIT,					/**< Initialization error. */
	BLE_EVENT_ERROR_UART,					/**< UART error. */
	BLE_EVENT_ERROR_FIFO,					/**< FIFO error. */
	BLE_EVENT_ERROR_CHECK,					/**< Check error. */
	BLE_EVENT_ERROR_PROTOCOL,				/**< Protocol error. */
	BLE_EVENT_MAX,							/**< Limitation. */

} ble_event_t;

/**
 * Bluetooth write commands.
 */
typedef enum {
	BLE_CMD_WR_END = 0x00,					/**< Termination indication. */
	BLE_CMD_WR_RST,							/**< Reset command. */
	BLE_CMD_WR_ADV_ON,						/**< Advertisement command. */
	BLE_CMD_WR_TRS,							/**< Transparent mode. */
	BLE_CMD_WR_MAX,							/**< Limitation. */
} ble_cmd_wr_t;

/**
 * Event callback.
 *
 * @param event Event according with @ref ble_event_t.
 */
typedef void (*ble_callback_event_t)(ble_event_t event);

/**
 * Protocol callback.
 *
 * @param	n		Number of bytes in the data field.
 * @param	cmd		Command byte.
 * @param	data	Data pointer.
 */
typedef void (*ble_callback_protocol_t)(uint8_t n, uint8_t cmd, uint8_t *data);

/**
 *  Driver register.
 */
typedef union {
		uint8_t value;						/**< Value. */
		struct {
			uint8_t en 			: 1;		/**< Enable. */
			uint8_t ok 			: 1;		/**< Successful indication. */
			uint8_t error 		: 1;		/**< Error indication. */
			uint8_t ready		: 1;		/**< Ready indication. */
			uint8_t connection 	: 1;		/**< Connection indication. */
			uint8_t trs 		: 1;		/**< Transparent mode. */
			uint8_t escape 		: 1;		/**< Escape indication. */
			uint8_t encrypt 	: 1;		/**< Encrypt indication. */
		} flags;
} ble_handle_reg_t;

/**
 * Driver handle.
 */
typedef struct {

	ble_handle_reg_t reg;					/**< Register. */
	ble_handle_state_t currentState;		/**< Current state. */
	ble_handle_state_t nextState;			/**< Next state. */
	volatile uint32_t counter;				/**< Generic counter. */
	uint32_t nextCounter;					/**< Next counter when the next state is used. */
	ble_event_t event;						/**< Next event when the next state is used. */
	ble_callback_event_t callbackEvent;		/**< Event callback. */
	ble_callback_protocol_t callbackProt;	/**< Protocol callback. */
	volatile buffer_t bufferRingRx;			/**< Uart RX buffer. */
	uint8_t bufferRx[BLE_BUFFER_RX_SIZE];	/**< Processing buffer. */
	uint16_t bufferIndex;					/**< Processing index. */

	char *cmdWr[BLE_CMD_WR_MAX];			/**< Commands to write. */

} ble_handle_t;

/*******************************************************************************
	EXTERNAL VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	PROTOTYPES:
*******************************************************************************/

void Ble_Init(ble_handle_t *h);
void Ble_Tick(ble_handle_t *h);
void Ble_Handle(ble_handle_t *h);
void Ble_Uart_Receive(ble_handle_t *h, const uint8_t data);

//----------------------------------------------------------------------------//

#endif /* BLE_H_ */

//----------------------------------------------------------------------------//
